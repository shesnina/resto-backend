require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
const db = require("./database/knex");
const morgan = require("morgan");

const logMiddleware = require("./middlewares/log.middleware");
// const restaurantsRoutes = require(`./routes/restaurantroutes`);
const users = require("./routes/user.route");
const restaurant = require("./routes/restaurant.route");
const adminuser = require("./routes/admin.user.routes");
const adminresto = require("./routes/admin.restaurant.routes");

app.use(cors());
app.use(express.json());
app.use(logMiddleware);
// app.use(restaurantsRoutes);
app.use(restaurant);
app.use(users);
app.use(adminuser);
app.use(adminresto);

const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Server already running on port ${PORT}`);
});
