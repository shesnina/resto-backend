const AdminRestaurantService = require("../services/admin.restaurant.service");

const findByNameOrAddress = async (req, res) => {
  const { search, filterByName, filterByPrice, filterByRate } = req.query;

  try {
    const getnameoraddres = await AdminRestaurantService.findByNameOrAddress(
      search,
      filterByName,
      filterByPrice,
      filterByRate
    );
    res.status(200).json({
      ...getnameoraddres,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const getAllRestaurant = async (req, res) => {
  try {
    const getAllresto = await AdminRestaurantService.getAllRestaurant();
    res.status(200).json({
      ...getAllresto,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const createResto = async (req, res) => {
  const dataResto = req.body;
  try {
    const createRestos = await AdminRestaurantService.createResto(dataResto);
    res.status(200).json({
      ...createRestos,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const updateResto = async (req, res) => {
  const dataResto = req.body;
  const { id } = req.params;
  console.log(id);
  try {
    const updateRestos = await AdminRestaurantService.updateResto(
      dataResto,
      id
    );
    res.status(200).json({
      ...updateRestos,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const createGallery = async (req, res) => {
  const { resto_gallery_name, resto_gallery_size } = req.body;
  const { id } = req.params;
  if (!req.files) {
    return res.status(400).json({ error: "missing file image" });
  }
  const pathList = req.files;
  if (!resto_gallery_name || !resto_gallery_size || pathList === 0) {
    return res.status(400).json({ error: "missing required fields" });
  }
  try {
    const newGallery = await AdminRestaurantService.createGallery({
      resto_gallery_name,
      resto_gallery_size,
      pathList,
      id,
    });
    res.status(201).json(newGallery);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" + error.message });
  }
};

const deleteResto = async (req, res) => {
  const { id } = req.params;
  console.log(id);
  try {
    const deleted = await AdminRestaurantService.deleteResto(id);
    res.status(200).json({
      status: `Success deleted user with id : ${id}`,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

module.exports = {
  getAllRestaurant,
  createResto,
  updateResto,
  createGallery,
  deleteResto,
  findByNameOrAddress,
};
