const restaurantService = require("../services/restaurant.service");
const ERROR_CONSTANT = require("../constants/error");
const RestoResponse = require("../dto/restoResponse");

const findByNameOrAddress = async (req, res) => {
  const { search, filterByName, filterByPrice, filterByRate } = req.query;

  try {
    const getnameoraddres = await restaurantService.findByNameOrAddress(
      search,
      filterByName,
      filterByPrice,
      filterByRate
    );
    res.status(200).json({
      ...getnameoraddres,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const getAllResto = async (req, res) => {
  const {
    page = 1,
    limit = 6,
    search,
    filterByName,
    filterByPrice,
    filterByRate,
  } = req.query;
  const offset = (page - 1) * limit;
  try {
    const { restaurants, totalRecords } = await restaurantService.getAllResto(
      limit,
      offset,
      search,
      filterByName,
      filterByPrice,
      filterByRate
    );
    const totalPages = Math.ceil(totalRecords / limit);
    const pagination = {
      page: page,
      totalRecords,
      totalPages,
    };
    let response = [];
    restaurants.map((resto) => {
      response.push(new RestoResponse(resto));
    });
    res.status(202).json({ results: response, ...pagination });
  } catch (error) {
    res.status(500).json({ error: ERROR_CONSTANT.INTERNAL_SERVER_ERROR });
  }
};

const getAllRestaurants = async (req, res) => {
  try {
    const getAllResto = await restaurantService.getAllRestaurants();
    res.status(200).json({
      ...getAllResto,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const createResto = async (req, res) => {
  const dataResto = req.body;
  try {
    const createRestos = await restaurantService.createResto(dataResto);
    res.status(200).json({
      ...createRestos,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const updateResto = async (req, res) => {
  const dataResto = req.body;
  const { id } = req.params;

  try {
    const updateRestos = await restaurantService.updateResto(dataResto, id);
    res.status(200).json({
      ...updateRestos,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const deleteResto = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(404).json({
      error: "sorry we could not find the id",
    });
  }
  try {
    const deleted = await restaurantService.deleteResto(id);
    res.status(200).json({
      status: `Success deleted user with id : ${id}`,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

module.exports = {
  findByNameOrAddress,
  getAllRestaurants,
  getAllResto,
  createResto,
  updateResto,
  deleteResto,
};
