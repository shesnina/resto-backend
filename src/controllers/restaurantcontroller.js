//Get All Restaurants
const db = require(`../db`);
const getRestaurants = async (req, res) => {
  try {
    const restaurantRatingData = await db.query(
      "select * from restaurants left join (select restaurant_id, COUNT(*), TRUNC(AVG(rating),1) as average_rating from reviews group by restaurant_id) reviews on restaurants.id = reviews.restaurant_id"
    );
    res.status(200).json({
      status: "Success",
      result: restaurantRatingData.rows.length,
      data: { restaurants: restaurantRatingData.rows },
    });
  } catch (err) {
    res.status(400).json({
      status: "Error code",
    });
  }
};

//Get Specific Restaurant
const getRestaurantsID = async (req, res) => {
  const restaurantID = parseInt(req.params.id);
  try {
    const restaurant = await db.query(
      `SELECT * from restaurants left join (select restaurant_id, COUNT(*), TRUNC(AVG(rating),1) as average_rating from reviews group by restaurant_id) reviews on restaurants.id = reviews.restaurant_id where id = $1`,
      [restaurantID]
    );
    const reviews = await db.query(
      `SELECT * FROM reviews where restaurant_id = $1`,
      [restaurantID]
    );
    res.status(200).json({
      status: "Success",
      data: { restaurants: restaurant.rows, reviews: reviews.rows },
    });
  } catch (err) {
    res.status(400).json({
      status: "Error code",
    });
  }
};

//Add New Restaurant
const addRestaurant = async (req, res) => {
  const restaurantAdded = req.body;
  try {
    const result = await db.query(
      `INSERT INTO restaurants ( name, location, price_range) values ($1, $2, $3) RETURNING *`,
      [
        restaurantAdded.name,
        restaurantAdded.location,
        restaurantAdded.price_range,
      ]
    );
    res.status(200).json({
      status: "Success",
      data: { restaurants: result.rows},
    });
  } catch (err) {
    res.status(400).json({
      status: "Error code",
    });
  }
};

//Update Restaurant
const updateRestaurant = async (req, res) => {
  const restaurantID = parseInt(req.params.id);
  const restaurantUpdated = req.body;
  try {
    const result = await db.query(
      `UPDATE restaurants SET name = $1, location = $2, price_range = $3 WHERE id = $4 RETURNING *`,
      [
        restaurantUpdated.name,
        restaurantUpdated.location,
        restaurantUpdated.price_range,
        restaurantID,
      ]
    );
    res.status(200).json({
      status: "Success",
      data: { restaurants: result.rows },
    });
  } catch (err) {
    res.status(400).json({
      status: "Error code",
    });
  }
};

//Delete Restaurants
const deleteRestaurant = async (req, res) => {
  const restaurantID = parseInt(req.params.id);
  try {
    const result = await db.query(`DELETE FROM restaurants WHERE id=$1`, [
      restaurantID,
    ]);
    res.status(204).json({
      status: `Success deleted restaurant with id : ${restaurantID}`,
    });
  } catch (err) {
    res.status(400).json({
      status: "Error code",
    });
  }
};

// Add Review
const addReview = async (req, res) => {
  const restaurantID = parseInt(req.params.id);
  try {
    const newReview = await db.query(
      "INSERT INTO reviews (restaurant_id, name, review, rating) values ($1, $2, $3, $4) returning *",
      [restaurantID, req.body.name, req.body.review, req.body.rating]
    );
    res.status(201).json({
      status: "success",
      data: {
        review: newReview.rows[0],
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "Error code",
    });
  }
};
module.exports = {
  getRestaurants,
  getRestaurantsID,
  addRestaurant,
  updateRestaurant,
  deleteRestaurant,
  addReview,
};
