const adminUserService = require("../services/admin.user.service");

const findbyName = async (req, res) => {
  const { name } = req.query;
  try {
    const findName = await adminUserService.findbyName(name);
    res.status(200).json({
      ...findName,
    });
    ƒ;
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const getUsers = async (req, res) => {
  try {
    const getUser = await adminUserService.getUsers();
    res.status(200).json({
      ...getUser,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const getUserById = async (req, res) => {
  const userID = parseInt(req.params.id);
  try {
    const getUserId = await adminUserService.getUserById(userID);
    res.status(200).json({
      ...getUserId,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const addUser = async (req, res) => {
  const userAdded = req.body;
  const { user_name, user_email, user_password, gender, user_img } = userAdded;
  try {
    const addUsers = await adminUserService.addUser(userAdded);
    res.status(200).json({
      ...userAdded,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const updateUser = async (req, res) => {
  const userID = parseInt(req.params.id);
  const userUpdated = req.body;
  try {
    const updateUsers = await adminUserService.updateUser(userUpdated, userID);
    res.status(200).json({
      ...updateUsers,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const deleteUser = async (req, res) => {
  const userID = parseInt(req.params.id);
  try {
    const delUserId = await adminUserService.deleteUser(userID);
    res.status(200).json({
      status: `Success deleted user with id : ${userID}`,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

module.exports = {
  getUsers,
  getUserById,
  addUser,
  updateUser,
  deleteUser,
  findbyName,
};
