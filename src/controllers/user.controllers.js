const bcrypt = require("bcrypt");
const fs = require("fs");
const jwt = require("jsonwebtoken");
const salt = bcrypt.genSaltSync(12);
const userService = require("../services/user.service");

const secret = process.env.SECRET_KEY;

const getUsers = async (req, res) => {
  try {
    const getUser = await userService.getUsers();
    res.status(200).json({ ...getUser });
  } catch (err) {
    res.status(500).json({
      status: "Error on database.",
    });
  }
};

const getUserById = async (req, res) => {
  const userID = parseInt(req.params.id);
  try {
    const user = await userService.getUserById(userID);
    if (user) {
      res.status(200).json({
        ...user,
      });
    } else {
      res.status(404).json({ error: "User not found." });
    }
  } catch (err) {
    res.status(500).json({
      status: "Error on database.",
    });
  }
};

const addUser = async (req, res) => {
  const { user_name, user_email } = req.body;
  try {
    const email = await userService.getUserByEmail(user_email);
    if (!user_name || !user_email || !req.body.user_password.trim()) {
      return res.status(400).json({ error: "Missing required fields." });
    } else if (email) {
      res.status(403).json({ error: "E-Mail already exist." });
    } else {
      const user = await userService.addUser({ user_name, user_email });
      if (user) {
        const user_id = user[0].user_id;
        const user_password = bcrypt.hashSync(req.body.user_password, salt);
        const password = await userService.addPassword({
          user_id,
          user_password,
        });
      }
      res.status(200).json({
        ...user,
      });
    }
  } catch (err) {
    res.status(500).json({
      status: "Error on database.",
    });
  }
};

const updateUser = async (req, res) => {
  const userID = parseInt(req.params.id);
  const { user_name, gender } = req.body;
  const user_img = req.file ? req.file.path : null;
  try {
    const user = await userService.getUserById(userID);
    if (!user_name || !gender) {
      return res.status(400).json({ error: "Missing required fields." });
    } else if (user) {
      const userUpdate = await userService.updateUser(
        { user_name, gender, user_img },
        userID
      );
      res.status(200).json({
        ...userUpdate,
      });
    } else {
      if (req.file) {
        fs.unlinkSync(req.file.path);
      }
      res.status(404).json({ error: "User not found." });
    }
  } catch (err) {
    res.status(500).json({
      status: "Error on database.",
    });
  }
};

const deleteUser = async (req, res) => {
  const userID = parseInt(req.params.id);
  try {
    const user = await userService.getUserById(userID);
    if (user) {
      const delUser = await userService.deleteUser(userID);
      res.status(200).json({
        status: `User ID ${userID} succesfully deleted.`,
      });
    } else {
      res.status(404).json({ error: "User not found." });
    }
  } catch (err) {
    res.status(500).json({
      status: "Error on database.",
    });
  }
};

const getLogin = async (req, res) => {
  const { user_email, user_password } = req.body;
  try {
    const user = await userService.getUserByEmail(user_email);
    if (!user_email || !user_password.trim()) {
      return res.status(400).json({ error: "Missing required fields." });
    } else if (user) {
      const password = await userService.getPassword(user.user_id);
      if (bcrypt.compareSync(user_password, password.user_password)) {
        const accessToken = jwt.sign(
          {
            id: user.user_id,
            username: user.user_name,
            usermail: user.user_email,
            role: user.role
          },
          secret
        );
        return res.json({ accessToken: accessToken });
      } else {
        res.status(200).json({ error: "Password is incorrect." });
      }
    } else {
      res.status(404).json({ error: "User not found." });
    }
  } catch (err) {
    res.status(500).json({
      status: "Error on database.",
    });
  }
};

const forgetPassword = async (req, res) => {
  const { user_email, user_token, new_password } = req.body;
  try {
    const user = await userService.getUserByEmail(user_email);
    if (!user_email) {
      return res.status(400).json({ error: "Missing required fields." });
    } else if (user) {
      const user_id = user.user_id;
      if (user_token && new_password.trim()) {
        const token = await userService.getPasswordToken(user.user_id);
        if (!token) {
          return res.status(404).json({ error: "Token not found." });
        } else if (token && token.password_token == user_token) {
          if (token.expired_at < new Date(new Date().toISOString())) {
            res.status(200).json({
              status: "Token has expired.",
            });
          } else {
            const checkPassword = await userService.checkPassword({
              user_id,
              new_password,
            });
            if (checkPassword === true) {
              res.status(200).json({
                status: "You have already used this password.",
              });
            } else {
              const user_password = bcrypt.hashSync(
                req.body.new_password,
                salt
              );
              const password = await userService.getPassword(user.user_id);
              const updatePasswordChanges =
                await userService.updatePasswordChanges(
                  password.users_password_id
                );
              const newPassword = await userService.addPassword({
                user_id,
                user_password,
              });
              const token = await userService.deletePasswordToken(user_id);
              res.status(200).json({
                status: "Password successfully updated.",
              });
            }
          }
        } else {
          res.status(200).json({
            status: "Token doesn't match.",
          });
        }
      } else if (user_token && !new_password.trim()) {
        return res.status(400).json({ error: "Missing required fields." });
      } else {
        const password_token = Math.random().toString(16).slice(2);
        const addPasswordToken = await userService.addPasswordToken({
          user_id,
          password_token,
        });
        res.status(200).json({
          status: "Success",
          data: addPasswordToken,
        });
      }
    } else {
      res.status(404).json({ error: "User not found." });
    }
  } catch (err) {
    res.status(500).json({
      status: "Error on database.",
    });
  }
};

const updatePassword = async (req, res) => {
  const { user_id, user_password, new_password } = req.body;
  try {
    const user = await userService.getUserById(user_id);
    if (!user_id || !user_password.trim() || !new_password.trim()) {
      return res.status(400).json({ error: "Missing required fields." });
    } else if (user) {
      const password = await userService.getPassword(user_id);
      if (bcrypt.compareSync(user_password, password.user_password)) {
        const checkPassword = await userService.checkPassword({
          user_id,
          new_password,
        });
        if (checkPassword === true) {
          res.status(200).json({
            status: "You have already used this password.",
          });
        } else {
          const user_password = bcrypt.hashSync(req.body.new_password, salt);
          const updatePasswordChanges = await userService.updatePasswordChanges(
            password.users_password_id
          );
          const newPassword = await userService.addPassword({
            user_id,
            user_password,
          });
          res.status(200).json({
            status: "Password successfully updated.",
          });
        }
      } else {
        res.status(200).json({ error: "Password is incorrect." });
      }
    } else {
      res.status(404).json({ error: "User not found." });
    }
  } catch (err) {
    res.status(500).json({
      status: "Error on database.",
    });
  }
};

const createReview = async (req, res) => {
  const { idResto, idUser } = req.params;
  const reviewAdd = req.body;
  try {
    const addReview = await userService.createReview(
      reviewAdd,
      idResto,
      idUser
    );
    res.status(200).json({
      ...addReview,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const createRating = async (req, res) => {
  const { idResto, idUser } = req.params;
  const ratingAdd = req.body;
  try {
    const addRating = await userService.createRating(
      ratingAdd,
      idResto,
      idUser
    );
    res.status(200).json({
      ...addRating,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const updateReview = async (req, res) => {
  const { id } = req.params;
  const ReviewUpdate = req.body;
  try {
    const addRating = await userService.updateReview(ReviewUpdate, id);
    res.status(200).json({
      ReviewUpdate,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

const updateRating = async (req, res) => {
  const { idResto, idUser } = req.params;
  const ratingAdd = req.body;
  try {
    const addRating = await userService.updateRating(
      ratingAdd,
      idResto,
      idUser
    );
    res.status(200).json({
      status: "Success",
      data: addRating,
    });
  } catch (err) {
    res.status(500).json({
      status: "Error on database check your connection",
    });
  }
};

module.exports = {
  getLogin,
  getUsers,
  getUserById,
  addUser,
  updateUser,
  deleteUser,
  forgetPassword,
  updatePassword,
  createReview,
  createRating,
  updateReview,
  updateRating,
};
