const express = require(`express`);
const adminUserControllers = require(`../controllers/admin.user.controllers`);
const router = express.Router();

router.get("/api/admin/users/", adminUserControllers.findbyName);
router.get("/api/admin/users/all", adminUserControllers.getUsers);
router.get("/api/admin/users/:id", adminUserControllers.getUserById);
router.post("/api/admin/users", adminUserControllers.addUser);
router.put("/api/admin/users/:id", adminUserControllers.updateUser);
router.delete("/api/admin/users/:id", adminUserControllers.deleteUser);

module.exports = router;
