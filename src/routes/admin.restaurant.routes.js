const express = require(`express`);
const adminRestoControllers = require(`../controllers/admin.restaurant.controllers`);
const router = express.Router();
const { imageUpload } = require("../middlewares/imageUploadMiddleware");
const { adminMiddleware } = require("../middlewares/authMiddleware");

router.get("/api/admin/restaurants", adminRestoControllers.findByNameOrAddress);
router.get(
  "/api/admin/restaurants/all",
  adminRestoControllers.getAllRestaurant
);
router.post("/api/admin/restaurants", adminRestoControllers.createResto);
router.post(
  "/api/admin/galleryresto/:id",
  imageUpload.array("resto_gallery_url", 5),
  // adminMiddleware,
  adminRestoControllers.createGallery
);
router.put("/api/admin/restaurants/:id", adminRestoControllers.updateResto);
router.delete("/api/admin/restaurants/:id", adminRestoControllers.deleteResto);

module.exports = router;
