const express = require(`express`);
const router = express.Router();
const restaurantController = require(`../controllers/restaurant.controller`);

router.get("/api/restaurants", restaurantController.findByNameOrAddress);
router.get("/allresto", restaurantController.getAllResto);
router.get("/api/restaurants/all", restaurantController.getAllRestaurants);
router.post("/api/restaurants", restaurantController.createResto);
router.put("/api/restaurants/:id", restaurantController.updateResto);
router.delete("/api/restaurants/:id", restaurantController.deleteResto);

module.exports = router;
