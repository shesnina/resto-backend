const express = require(`express`);

const router = express.Router();
const restaurantcontrollers = require(`../controllers/restaurantcontroller`);
router.get(`/api/restaurants`, restaurantcontrollers.getRestaurants);
router.get(`/api/restaurants/:id`, restaurantcontrollers.getRestaurantsID);
router.post(`/api/restaurants`, restaurantcontrollers.addRestaurant);
router.put(
  `/api/restaurants/:id/update`,
  restaurantcontrollers.updateRestaurant
);
router.post(`/api/restaurants/:id/reviews`, restaurantcontrollers.addReview); 
router.delete(`/api/restaurants/:id`, restaurantcontrollers.deleteRestaurant);
module.exports = router;
