const express = require(`express`);
const router = express.Router();
const userController = require(`../controllers/user.controllers`);
const { imageUpload } = require("../middlewares/imageUploadMiddleware");

router.get("/api/users/all", userController.getUsers);
router.get("/api/users/:id", userController.getUserById);
router.post("/api/users", userController.addUser);
router.post("/api/users/login", userController.getLogin);
router.post("/api/users/password", userController.updatePassword);
router.post("/api/users/forgetpassword", userController.forgetPassword);
router.put(
  "/api/users/:id",
  imageUpload.single("user_img"),
  userController.updateUser
);
router.delete("/api/users/:id", userController.deleteUser);

router.post("/users/review/:idResto/:idUser", userController.createReview);
router.post("/users/rating/:idResto/:idUser", userController.createRating);
router.put("/users/review/:id", userController.updateReview);
router.put("/users/rating/:idResto/:idUser", userController.updateRating);

module.exports = router;
