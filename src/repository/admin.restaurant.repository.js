const knex = require("../database/knex");
const { uploadToCloud } = require("../middlewares/imageUploadMiddleware");

const findByNameOrAddress = async (
  search,
  filterByName,
  filterByPrice,
  filterByRate
) => {
  try {
    const ResultSearch = await knex("restaurants")
      .select(
        "r.*",
        "fc.food_category_name",
        "fc.food_category_id",
        "re.message_review",
        "re.created_at as date_review",
        "re.updated_at as update_review",
        "ra.rating",
        "ra.created_at as date_rating",
        "ra.updated_at as update_rating",
        "u.user_name"
      )
      .from("restaurants as r")
      .leftJoin(
        `food_category as fc`,
        `fc.food_category_id`,
        `r.food_category_id `
      )
      .leftJoin(`reviews as re`, `re.restaurant_id`, `r.restaurant_id`)
      .leftJoin(`users as u`, `re.user_id`, `u.user_id`)
      .leftJoin(`ratings as ra `, `u.user_id`, `ra.user_id`)
      // .whereILike("r.restaurant_name", `%${search}%`)
      // .orWhereILike("r.restaurant_address", `%${search}%`)
      // .whereRaw(`AND fc.food_category_name = %${filterByName}%`)
      // // .whereILike("fc.food_category_name", `%${filterByName}%`)
      .modify((builder) => {
        if (search) {
          builder.where("restaurant_name", "ilike", `%${search}%`);
          builder.orWhere("restaurant_address", "ilike", `%${search}%`);
        }
      })
      .modify((builder) => {
        if (filterByName) {
          builder.andWhere(
            "fc.food_category_name",
            "ilike",
            `%${filterByName}%`
          );
        }
      })
      .modify((builder) => {
        if (filterByPrice) {
          builder.andWhere("r.price_range", ">=", `${filterByPrice}`);
        }
      })
      .modify((builder) => {
        if (filterByRate) {
          builder.andWhere("ra.rating", "=", `${filterByRate}`);
        }
      })
      .returning("*");
    return ResultSearch;
  } catch (err) {
    throw new Error(err);
  }
};

const getAllRestaurants = async () => {
  try {
    const ResultSearch = await knex
      .select(
        "r.*",
        "fc.food_category_name",
        "fc.food_category_id",
        "re.message_review",
        "re.created_at as date_review",
        "re.updated_at as update_review",
        "ra.rating",
        "ra.created_at as date_rating",
        "ra.updated_at as update_rating",
        "u.user_name"
      )
      .from("restaurants as r")
      .leftJoin(
        `food_category as fc`,
        `fc.food_category_id`,
        `r.food_category_id `
      )
      .leftJoin(`reviews as re`, `re.restaurant_id`, `r.restaurant_id`)
      .leftJoin(`users as u`, `re.user_id`, `u.user_id`)
      .leftJoin(`ratings as ra `, `u.user_id`, `ra.user_id`)
      .returning("*");
    return ResultSearch;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const createResto = async (dataResto) => {
  try {
    const resto = await knex(`restaurants`)
      .insert({ ...dataResto })
      .returning("*");
    return resto;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const updateResto = async (dataResto, id) => {
  console.log(id);
  try {
    const updaterestos = await knex(`restaurants`)
      .where({ restaurant_id: id })
      .update(dataResto)
      .returning("*");
    return updaterestos;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const createGallery = async (req) => {
  const { resto_gallery_name, resto_gallery_size, pathList, id } = req;
  try {
    const uploadPromises = pathList.map(async (path) => {
      const response = await uploadToCloud(path);
      return response;
    });
    const urlList = await Promise.all(uploadPromises);
    console.log(urlList);
    if (urlList.length > 0) {
      const [resto_gallery] = await knex("resto_gallery")
        .insert({
          resto_gallery_name,
          resto_gallery_size,
          restaurant_id: id,
        })
        .returning("resto_gallery_id");
      const insertedUrls = await knex("gallery_urls")
        .insert(
          urlList.map((url) => ({
            url,
            resto_gallery_id: resto_gallery.resto_gallery_id,
          }))
        )
        .returning("url");
      console.log(insertedUrls);
      return resto_gallery;
    }
    throw new Error("failed to upload image");
  } catch (err) {
    throw new Error("failed to create gallery: " + err.message);
  }
};

const deleteResto = async (id) => {
  console.log(id);
  try {
    const delResto = await knex(`restaurants`)
      .where({ restaurant_id: id })
      .del()
      .returning("*");
    return delResto;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

module.exports = {
  getAllRestaurants,
  createResto,
  updateResto,
  createGallery,
  deleteResto,
  findByNameOrAddress,
};
