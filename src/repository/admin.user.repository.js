const knex = require("../database/knex");

const findbyName = async (req) => {
  try {
    const Login = await knex
      .select("users.*", "users_password.*")
      .from("users")
      .leftJoin("users_password", "users.user_id", "users_password.user_id")
      .whereLike("users.user_name", req)
      .returning("*");
    return Login;
  } catch (err) {
    throw new Error(err);
  }
};

const getUsers = async () => {
  try {
    const userAll = await knex
      .select("users.*", "users_password.*")
      .from("users")
      .leftJoin("users_password", "users.user_id", "users_password.user_id")
      .returning("*");
    return userAll;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const getUserById = async (req) => {
  try {
    const user = await knex("users").where({ user_id: req }).first();
    return user;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const addUser = async (req) => {
  const { user_name, user_email, user_password, gender, user_img } = req;
  try {
    const id = await knex
      .insert({
        user_name: user_name,
        user_email: user_email,
        gender: gender,
        user_img: user_img,
      })
      .returning("user_id")
      .into("users")
      .then(function (id) {
        const convert = JSON.stringify(id[0].user_id);
        const convertint = parseInt(convert);
        return knex("users_password").insert({
          user_password: user_password,
          user_id: convertint,
        });
      });
    return id;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const updateUser = async (reqUpdate, reqId) => {
  try {
    const resultUpdate = await knex("users")
      .where({ user_id: reqId })
      .update({ ...reqUpdate })
      .returning("*");
    return resultUpdate;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const deleteUser = async (req) => {
  try {
    const delId = await knex("users").where({ user_id: req }).del();
    return delId;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

module.exports = {
  getUsers,
  getUserById,
  addUser,
  updateUser,
  deleteUser,
  findbyName,
};
