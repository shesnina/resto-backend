const bcrypt = require('bcrypt');
const knex = require("../database/knex");
const { uploadToCloud } = require("../middlewares/imageUploadMiddleware");

const authenticate = async (payload, done) => {
  try {
    const user = await getUserById(payload.id);
    return done(null, user);
  } catch (error) {
    return done(error, { error: unauthorized });
  }
};

const getUsers = async () => {
  try {
    const userAll = await knex.select(`users.*`).from("users");
    return userAll;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const getUserById = async (req) => {
  try {
    const user = await knex("users").where({ user_id: req }).first();
    return user;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const getUserByEmail = async (req) => {
  try {
    const user = await knex("users").where({ user_email: req }).first();
    return user;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const addUser = async (req) => {
  try {
    const user = await knex("users")
      .insert({ ...req })
      .returning("*");
    return user;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const updateUser = async (reqUpdate, reqId) => {
  const { user_name, gender, user_img } = reqUpdate;
  try {
    const url = await uploadToCloud(user_img);
    if (url) {
      const resultUpdate = await knex("users")
      .where({ user_id: reqId })
      .update({ user_name, gender, user_img: url })
      .returning("*");
      return resultUpdate;
    }
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const deleteUser = async (req) => {
  try {
    const delPass = await knex("users_password").where({ user_id: req }).del();
    const delUser = await knex("users").where({ user_id: req }).del();
    return delUser;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const addPassword = async (req) => {
  try {
    const password = await knex("users_password")
      .insert({ ...req });
    return password;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const addPasswordToken = async (req) => {
  try {
    const token = await knex("users_password_token")
      .insert({ ...req })
      .returning("*");
    return token;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const checkPassword = async (req) => {
  try {
    const password = await knex("users_password").where({ user_id: req.user_id });
    for (let i = 0; i < password.length; i++) {
      const resultOfCompare = await bcrypt.compareSync(req.new_password, password[i].user_password);
      if (resultOfCompare) {
        passMatched = true;
        break;
      } else {
        passMatched = false;
      }
    }
    return passMatched;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const deletePasswordToken = async (req) => {
  try {
    const token = await knex("users_password_token").where({ user_id: req }).del();
    return token;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const updatePasswordChanges = async (req) => {
  try {
    const resultUpdate = await knex("users_password")
      .where({ users_password_id: req })
      .update({ changed_at: new Date(new Date().toISOString()) })
      .returning("*");
    return resultUpdate;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const getPassword = async (req) => {
  try {
    const password = await knex("users_password").where({ user_id: req }).orderBy('created_at', 'desc').first();
    return password;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const getPasswordToken = async (req) => {
  try {
    const token = await knex("users_password_token").where({ user_id: req }).orderBy('created_at', 'desc').first();
    return token;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const createReview = async (reviewAdd, idResto, idUser) => {
  try {
    const id = await knex("reviews")
      .insert({
        ...reviewAdd,
        user_id: idUser,
        restaurant_id: idResto,
        created_at: new Date(),
      })
      .returning("*");
    return id;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const createRating = async (ratingAdd, idResto, idUser) => {
  try {
    const id = await knex("ratings")
      .insert({
        ...ratingAdd,
        user_id: idUser,
        restaurant_id: idResto,
        created_at: new Date(),
      })
      .returning("*");
    return id;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const updateReview = async (ReviewUpdate, id) => {
  try {
    const resultUpdate = await knex("reviews")
      .where({ review_id: id })
      .update({ ...ReviewUpdate, updated_at: new Date() })
      .returning("*");
    return resultUpdate;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const updateRating = async (ratingAdd, idResto, idUser) => {
  try {
    const id = await knex("ratings")
      .where({
        user_id: idUser,
        restaurant_id: idResto,
      })
      .update({ ...ratingAdd, updated_at: new Date() })
      .returning("*");
    return id;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

module.exports = {
  authenticate,
  getUsers,
  getUserById,
  getUserByEmail,
  addUser,
  updateUser,
  deleteUser,
  addPassword,
  addPasswordToken,
  checkPassword,
  deletePasswordToken,
  updatePasswordChanges,
  getPassword,
  getPasswordToken,
  createReview,
  createRating,
  updateReview,
  updateRating,
};
