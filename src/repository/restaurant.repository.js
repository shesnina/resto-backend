const knex = require("../database/knex");

const findByNameOrAddress = async (
  search,
  filterByName,
  filterByPrice,
  filterByRate
) => {
  try {
    const ResultSearch = await knex("restaurants")
      .select(
        "r.*",
        "fc.food_category_name",
        "fc.food_category_id",
        "re.message_review",
        "re.created_at as date_review",
        "re.updated_at as update_review",
        "ra.rating",
        "ra.created_at as date_rating",
        "ra.updated_at as update_rating",
        "u.user_name"
      )
      .from("restaurants as r")
      .leftJoin(
        `food_category as fc`,
        `fc.food_category_id`,
        `r.food_category_id `
      )
      .leftJoin(`reviews as re`, `re.restaurant_id`, `r.restaurant_id`)
      .leftJoin(`users as u`, `re.user_id`, `u.user_id`)
      .leftJoin(`ratings as ra `, `u.user_id`, `ra.user_id`)
      // .whereILike("r.restaurant_name", `%${search}%`)
      // .orWhereILike("r.restaurant_address", `%${search}%`)
      // .whereRaw(`AND fc.food_category_name = %${filterByName}%`)
      // // .whereILike("fc.food_category_name", `%${filterByName}%`)
      .modify((builder) => {
        if (search) {
          builder.where("restaurant_name", "ilike", `%${search}%`);
          builder.orWhere("restaurant_address", "ilike", `%${search}%`);
        }
      })
      .modify((builder) => {
        if (filterByName) {
          builder.andWhere(
            "fc.food_category_name",
            "ilike",
            `%${filterByName}%`
          );
        }
      })
      .modify((builder) => {
        if (filterByPrice) {
          builder.andWhere("r.price_range", ">=", `${filterByPrice}`);
        }
      })
      .modify((builder) => {
        if (filterByRate) {
          builder.andWhere("ra.rating", "=", `${filterByRate}`);
        }
      })
      .returning("*");
    return ResultSearch;
  } catch (err) {
    throw new Error(err);
  }
};

const getAllResto = async (
  limit,
  offset,
  search,
  filterByName,
  filterByPrice,
  filterByRate
) => {
  try {
    const query = await knex("restaurants")
      .select(
        "r.restaurant_id",
        "r.restaurant_name",
        "r.restaurant_address",
        "r.deleted_at",
        "r.price_range",
        "s.rating",
        "fc.food_category_name"
      )
      .from("restaurants as r")
      .leftJoin("ratings as s", "s.restaurant_id", "r.restaurant_id")
      .leftJoin(
        `food_category as fc`,
        `fc.food_category_id`,
        `r.food_category_id `
      )
      .whereNull("deleted_at")
      .modify((builder) => {
        if (search) {
          builder.where("restaurant_name", "ilike", `%${search}%`);
        }
      })
      .modify((builder) => {
        if (filterByName) {
          builder.andWhere(
            "fc.food_category_name",
            "ilike",
            `%${filterByName}%`
          );
        }
      })
      .modify((builder) => {
        if (filterByPrice) {
          builder.andWhere("r.price_range", ">=", `${filterByPrice}`);
        }
      })
      .modify((builder) => {
        if (filterByRate) {
          builder.andWhere("s.rating", "=", `${filterByRate}`);
        }
      })
      .limit(limit)
      .offset(offset)
      .orderBy("rating");

    const countQuery = knex("restaurants").whereNull("deleted_at");
    if (search) {
      countQuery.where("restaurant_name", "ilike", `%${search}%`);
    }
    const [restaurants, [{ totalRecords }]] = await Promise.all([
      query,
      countQuery.count("* as totalRecords"),
    ]);
    return { restaurants, totalRecords: parseInt(totalRecords, 10) };
  } catch (error) {
    console.error(error);
    throw new Error(error);
  }
};

const getAllRestaurants = async () => {
  try {
    const AllResto = await knex
      .select(
        "r.*",
        "fc.food_category_name",
        "fc.food_category_id",
        "re.message_review",
        "re.created_at as date_review",
        "re.updated_at as update_review",
        "ra.rating",
        "ra.created_at as date_rating",
        "ra.updated_at as update_rating",
        "u.user_name"
      )
      .from("restaurants as r")
      .leftJoin(
        `food_category as fc`,
        `fc.food_category_id`,
        `r.food_category_id `
      )
      .leftJoin(`reviews as re`, `re.restaurant_id`, `r.restaurant_id`)
      .leftJoin(`users as u`, `re.user_id`, `u.user_id`)
      .leftJoin(`ratings as ra `, `u.user_id`, `ra.user_id`)
      .returning("*");
    return AllResto;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const createResto = async (dataResto) => {
  try {
    const resto = await knex(`restaurants`)
      .insert({ ...dataResto })
      .returning("*");
    return resto;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const updateResto = async (dataResto, id) => {
  console.log(id);
  try {
    const updaterestos = await knex(`restaurants`)
      .where({ restaurant_id: id })
      .update(dataResto)
      .returning("*");
    return updaterestos;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

const deleteResto = async (id) => {
  console.log(id);
  try {
    const DelResto = await knex(`restaurants`)
      .where({ restaurant_id: id })
      .del()
      .returning("*");
    return DelResto;
  } catch (err) {
    console.log(err);
    throw new Error(err);
  }
};

module.exports = {
  findByNameOrAddress,
  getAllResto,
  getAllRestaurants,
  createResto,
  updateResto,
  deleteResto,
};
