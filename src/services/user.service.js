const userRepository = require("../repository/user.repository");

const getUsers = async () => {
  try {
    return await userRepository.getUsers();
  } catch (err) {
    throw err;
  }
};

const getUserById = async (req) => {
  try {
    return await userRepository.getUserById(req);
  } catch (err) {
    throw err;
  }
};

const getUserByEmail = async (req) => {
  try {
    return await userRepository.getUserByEmail(req);
  } catch (err) {
    throw err;
  }
};

const addUser = async (req) => {
  try {
    return await userRepository.addUser(req);
  } catch (err) {
    throw err;
  }
};

const updateUser = async (reqUpdate, reqId) => {
  try {
    return await userRepository.updateUser(reqUpdate, reqId);
  } catch (err) {
    throw err;
  }
};

const deleteUser = async (req) => {
  try {
    return await userRepository.deleteUser(req);
  } catch (err) {
    throw err;
  }
};

const addPassword = async (req) => {
  try {
    return await userRepository.addPassword(req);
  } catch (err) {
    throw err;
  }
};

const addPasswordToken = async (req) => {
  try {
    return await userRepository.addPasswordToken(req);
  } catch (err) {
    throw err;
  }
};

const checkPassword = async (req) => {
  try {
    return await userRepository.checkPassword(req);
  } catch (err) {
    throw err;
  }
};

const deletePasswordToken = async (req) => {
  try {
    return await userRepository.deletePasswordToken(req);
  } catch (err) {
    throw err;
  }
};

const updatePasswordChanges = async (req) => {
  try {
    return await userRepository.updatePasswordChanges(req);
  } catch (err) {
    throw err;
  }
};

const getPassword = async (req) => {
  try {
    return await userRepository.getPassword(req);
  } catch (err) {
    throw err;
  }
};

const getPasswordToken = async (req) => {
  try {
    return await userRepository.getPasswordToken(req);
  } catch (err) {
    throw err;
  }
};

const createReview = async (reviewAdd, idResto, idUser) => {
  try {
    return await userRepository.createReview(reviewAdd, idResto, idUser);
  } catch (err) {
    throw err;
  }
};

const createRating = async (ratingAdd, idResto, idUser) => {
  try {
    return await userRepository.createRating(ratingAdd, idResto, idUser);
  } catch (err) {
    throw err;
  }
};

const updateReview = async (reviewUpdate, idResto, idUser) => {
  try {
    return await userRepository.updateReview(reviewUpdate, idResto, idUser);
  } catch (err) {
    throw err;
  }
};

const updateRating = async (ratingAdd, idResto, idUser) => {
  try {
    return await userRepository.updateRating(ratingAdd, idResto, idUser);
  } catch (err) {
    throw err;
  }
};
module.exports = {
  getUsers,
  getUserById,
  getUserByEmail,
  addUser,
  updateUser,
  deleteUser,
  addPassword,
  addPasswordToken,
  checkPassword,
  deletePasswordToken,
  updatePasswordChanges,
  getPassword,
  getPasswordToken,
  createReview,
  createRating,
  updateReview,
  updateRating
};
