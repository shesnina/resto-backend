const adminUserRepsitory = require("../repository/admin.user.repository");

const findbyName = async (name) => {
  try {
    return await adminUserRepsitory.findbyName(name);
  } catch (err) {
    throw err;
  }
};

const getUsers = async () => {
  try {
    return await adminUserRepsitory.getUsers();
  } catch (err) {
    throw err;
  }
};

const getUserById = async (req) => {
  try {
    return await adminUserRepsitory.getUserById(req);
  } catch (err) {
    throw err;
  }
};

const addUser = async (req) => {
  try {
    return await adminUserRepsitory.addUser(req);
  } catch (err) {
    throw err;
  }
};

const updateUser = async (reqUpdate, reqId) => {
  try {
    return await adminUserRepsitory.updateUser(reqUpdate, reqId);
  } catch (err) {
    throw err;
  }
};

const deleteUser = async (req) => {
  try {
    return await adminUserRepsitory.deleteUser(req);
  } catch (err) {
    throw err;
  }
};

module.exports = {
  getUsers,
  getUserById,
  addUser,
  updateUser,
  deleteUser,
  findbyName,
};
