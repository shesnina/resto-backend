const restaurantRepository = require("../repository/restaurant.repository");

const findByNameOrAddress = async (
  search,
  filterByName,
  filterByPrice,
  filterByRate
) => {
  try {
    return await restaurantRepository.findByNameOrAddress(
      search,
      filterByName,
      filterByPrice,
      filterByRate
    );
  } catch (err) {
    throw err;
  }
};

const getAllResto = async (
  limit,
  offset,
  search,
  filterByName,
  filterByPrice,
  filterByRate
) => {
  try {
    const { restaurants, totalRecords } =
      await restaurantRepository.getAllResto(
        limit,
        offset,
        search,
        filterByName,
        filterByPrice,
        filterByRate
      );
    return { restaurants, totalRecords };
  } catch (error) {
    throw error;
  }
};

const getAllRestaurants = async () => {
  try {
    return await restaurantRepository.getAllRestaurants();
  } catch (err) {
    throw err;
  }
};

const createResto = async (dataResto) => {
  try {
    return await restaurantRepository.createResto(dataResto);
  } catch (err) {
    throw err;
  }
};

const updateResto = async (dataResto, id) => {
  try {
    return await restaurantRepository.updateResto(dataResto, id);
  } catch (err) {
    throw err;
  }
};

const deleteResto = async (id) => {
  try {
    return await restaurantRepository.deleteResto(id);
  } catch (err) {
    throw err;
  }
};

module.exports = {
  findByNameOrAddress,
  getAllRestaurants,
  getAllResto,
  createResto,
  updateResto,
  deleteResto,
};
