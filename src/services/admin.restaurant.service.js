const AdminRestaurantRepsitory = require("../repository/admin.restaurant.repository");

const findByNameOrAddress = async (
  search,
  filterByName,
  filterByPrice,
  filterByRate
) => {
  try {
    return await AdminRestaurantRepsitory.findByNameOrAddress(
      search,
      filterByName,
      filterByPrice,
      filterByRate
    );
  } catch (err) {
    throw err;
  }
};

const getAllRestaurant = async () => {
  try {
    return await AdminRestaurantRepsitory.getAllRestaurants();
  } catch (err) {
    throw err;
  }
};

const createResto = async (dataResto) => {
  try {
    return await AdminRestaurantRepsitory.createResto(dataResto);
  } catch (err) {
    throw err;
  }
};

const updateResto = async (dataResto, id) => {
  try {
    return await AdminRestaurantRepsitory.updateResto(dataResto, id);
  } catch (err) {
    throw err;
  }
};

const createGallery = (req) => {
  return AdminRestaurantRepsitory.createGallery(req);
};

const deleteResto = async (id) => {
  try {
    return await AdminRestaurantRepsitory.deleteResto(id);
  } catch (err) {
    throw err;
  }
};

module.exports = {
  getAllRestaurant,
  createResto,
  updateResto,
  createGallery,
  deleteResto,
  findByNameOrAddress,
};
