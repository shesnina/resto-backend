const customError = {
  IMAGE_EXTENSION_ERROR: "only .png, .jpg, and .jpeg format allowed!!",
  INTERNAL_SERVER_ERROR: "Internal Server Error",
  USERNAME_ALREADY_EXIST_ERROR: "username already exist",
};

module.exports = customError;
