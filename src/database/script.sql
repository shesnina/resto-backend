================== script for gold project =========================
CREATE TABLE products (
id INT,
name VARCHAR(50),
price INT,
on_sale boolean
);

CREATE TABLE restaurants (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(50) NOT NULL,
    price_range INT NOT NULL check(price_range>=1 and price_range<=5)
);

INSERT INTO restaurants ( name, location, price_range) values ( 'kfc', 'kediri',4);

CREATE TABLE reviews (
id BIGSERIAL NOT NULL PRIMARY KEY,
restaurant_id  BIGINT NOT NULL REFERENCES restaurants(id) ,
name VARCHAR(50) NOT NULL,
review TEXT NOT NULL,
rating INT check (rating>=1 and rating <=5) 
);

INSERT INTO reviews ( restaurant_id, name, review, rating) values (1, 'Jo','Great Restaurant', 5);


================== new table of platinum project team1 =========================

DROP TABLE IF EXISTS ratings;
DROP TABLE IF EXISTS resto_gallery;
DROP TABLE IF EXISTS restaurants;
DROP TABLE IF EXISTS food_category;
DROP TABLE IF EXISTS reviews;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS users_password;

CREATE DATABASE restofinder;

DROP DATABASE restofinder;

CREATE TABLE IF NOT EXISTS food_category(
    food_category_id SERIAL PRIMARY KEY,
    food_category_name VARCHAR(100)
);

INSERT INTO food_category (food_category_name)VALUES('Vegetarian'),	
					('Halal'),	
					('Non-Halal');	

CREATE TABLE IF NOT EXISTS users(
    user_id SERIAL PRIMARY KEY,
    user_name VARCHAR(50)NOT NULL,
    user_email VARCHAR(60) NOT NULL,
    gender VARCHAR(6),
    registered_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_img VARCHAR(100),
    role VARCHAR(50) NOT NULL DEFAULT 'User'
);

INSERT INTO users (user_name,user_email,gender,registered_at,user_img,role)VALUES('baharudin','baharudin@loli.com','male','2020-01-01 19:10:25-07','smileludin.jpg','user'),
										('lolita','lolita@loli.com','female','2020-01-02 19:10:25-07','lolita.jpg','user'),
										('indun','celialcelau@loli.com','female','2020-01-03 19:10:25-07','indun.jpg','user'),
										('ucup','cup@loli.com','male','2020-01-04 19:10:25-07','cupup.jpg','user'),
										('admin','mimin@con','unknow','2020-01-05 19:10:25-07','invs.jpg','admin');

CREATE TABLE IF NOT EXISTS users_password(
    users_password_id SERIAL PRIMARY KEY,
    user_password VARCHAR(150) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed_at TIMESTAMP,
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES users (user_id)
);

INSERT INTO users_password (user_password,created_at,changed_at,user_id)VALUES('baharudinjadikaya','2020-01-01 19:10:25-07',NULL,1),
							('loli1231','2020-01-02 19:10:25-07',NULL,2),
							('dun123566','2020-01-03 19:10:25-07',NULL,3),
							('cup298123','2020-01-04 19:10:25-07','2020-01-16 19:10:25-07',4),
							('ikal123','2020-01-05 19:10:25-07','2020-01-16 19:10:25-07',4),
							('OKASDAS123','2020-01-05 19:10:25-07','2020-01-16 19:10:25-07',4);

CREATE TABLE IF NOT EXISTS users_password_token(
    users_password_token_id SERIAL PRIMARY KEY,
    password_token VARCHAR(150) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    expired_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP + interval '30 minutes',
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES users (user_id)
);


CREATE TABLE IF NOT EXISTS restaurants(
    restaurant_id SERIAL PRIMARY KEY,
    restaurant_name VARCHAR(100) NOT NULL,
    restaurant_email VARCHAR(50) NOT NULL,
    restaurant_password VARCHAR(50) NOT NULL,
    restaurant_address VARCHAR(500),
    restaurant_phone VARCHAR(50),
    restaurant_about VARCHAR(800),
    restaurant_facility VARCHAR(255),
    oprational_time VARCHAR(150),
    price_range INT NOT NULL check(price_range>=100000 and price_range<=500000),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    is_deleted boolean,
    food_category_id INT,
    FOREIGN KEY (food_category_id) REFERENCES food_category (food_category_id)
);

INSERT INTO restaurants (restaurant_name,restaurant_email,restaurant_password,restaurant_address,restaurant_phone,restaurant_about,restaurant_facility,oprational_time,price_range,created_at,updated_at,deleted_at,is_deleted,food_category_id)VALUES('ABCD limadasar','abcd@acb.com','ookkk123456','Located in the heart of the city, on the 56th floor of BCA Tower - Grand Indonesia Shopping Town, Thamrin.','021-981234','SKYE is established to be a Lifestyle Resort in the Sky and an iconic landmark for the city, providing a unique atmosphere for a quick getaway for Jakartans from their daily hectic lives. ','Reservations, Private Dining, Seating, Parking Available, Validated Parking, Valet Parking, Wheelchair Accessible, Serves Alcohol, Full Bar, Accepts Mastercard, Accepts Visa, Cash Only, Free Wifi, Accepts Credit Cards, Table Service, Live Music','Office hours  10 AM - 10 PM Weekend 10 AM - 10 PM Public Holidays 10 AM  - 8 PM','500000','2020-01-01 19:10:25-07',NULL,NULL,FALSE,1),
('ABD KUYKUY','abcd@acb.com','ookkk123456','Located in the heart of the city, on the 56th floor of BCA Tower - Grand Indonesia Shopping Town, Thamrin.','021-981234','SKYE is established to be a Lifestyle Resort in the Sky and an iconic landmark for the city, providing a unique atmosphere for a quick getaway for Jakartans from their daily hectic lives. ','Reservations, Private Dining, Seating, Parking Available, Validated Parking, Valet Parking, Wheelchair Accessible, Serves Alcohol, Full Bar, Accepts Mastercard, Accepts Visa, Cash Only, Free Wifi, Accepts Credit Cards, Table Service, Live Music','Office hours  10 AM - 10 PM Weekend 10 AM - 10 PM Public Holidays 10 AM  - 8 PM','400000','2020-01-01 19:10:25-07',NULL,NULL,FALSE,1),													
('LIMA ENAM9','abcd@acb.com','ookkk123456','Located in the heart of the city, on the 56th floor of BCA Tower - Grand Indonesia Shopping Town, Thamrin.','021-981234','SKYE is established to be a Lifestyle Resort in the Sky and an iconic landmark for the city, providing a unique atmosphere for a quick getaway for Jakartans from their daily hectic lives. ','Reservations, Private Dining, Seating, Parking Available, Validated Parking, Valet Parking, Wheelchair Accessible, Serves Alcohol, Full Bar, Accepts Mastercard, Accepts Visa, Cash Only, Free Wifi, Accepts Credit Cards, Table Service, Live Music','Office hours  10 AM - 10 PM Weekend 10 AM - 10 PM Public Holidays 10 AM  - 8 PM','400000','2020-01-01 19:10:25-07',NULL,NULL,TRUE,1);


CREATE TABLE IF NOT EXISTS resto_gallery(
    resto_gallery_id SERIAL PRIMARY KEY,
    resto_gallery_name VARCHAR(150),
    resto_gallery_size VARCHAR(150),
    restaurant_id INT,
    FOREIGN KEY (restaurant_id) REFERENCES restaurants (restaurant_id)
);

INSERT INTO resto_gallery (resto_gallery_name,resto_gallery_size,restaurant_id)VALUES('ABCDFGK.jpg','8mb',1),
								('ABCDFGK1.jpg','2mb',1),
								('ABCDFGK2.jpg','3mb',1),
								('ABCDFGK3.jpg','10mb',1),
								('ABCDFGK4.jpg','20mb',1),
								('ABCDFGK5.jpg','38mb',1);

CREATE TABLE gallery_urls (
    id SERIAL PRIMARY KEY,
    resto_gallery_id integer,
    url text
);
ALTER TABLE gallery_urls
    ADD CONSTRAINT gallery_urls_resto_gallery_id_fkey FOREIGN KEY (resto_gallery_id) REFERENCES resto_gallery (resto_gallery_id);

CREATE TABLE IF NOT EXISTS ratings(
    rating_id SERIAL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP,
    rating INT check (rating>=1 and rating <=5),
    user_id INT,
    restaurant_id INT,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (restaurant_id) REFERENCES restaurants (restaurant_id)
);

INSERT INTO ratings (created_at,updated_at,rating,restaurant_id,user_id)VALUES('2020-01-01',NULL,2,1,1),
										('2020-01-01 19:10:25-07',NULL,3,3,4),
										('2020-01-01 19:10:25-07',NULL,3,1,3);


CREATE TABLE IF NOT EXISTS reviews(
    review_id SERIAL PRIMARY KEY,
    message_review VARCHAR(255), 
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
    updated_at TIMESTAMP, 
    deleted_at TIMESTAMP, 
    is_deleted BOOLEAN,  
    user_id INT,
    restaurant_id INT,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (restaurant_id) REFERENCES restaurants (restaurant_id)
);

INSERT INTO reviews (message_review,created_at,updated_at,deleted_at,is_deleted,restaurant_id,user_id)VALUES('aku selalu di depan','2020-01-01 10:20:00',NULL,NULL,FALSE,1,1),
												('ok aja dah akh ','2020-01-01 10:20:00','2020-05-14 10:20:00',NULL,FALSE,1,3),
												('jangan gila dong','2020-01-01 10:20:00','2020-05-14 10:20:00','2020-05-15 10:20:00',TRUE,1,4),
												('123123abcsad','2020-01-01 10:20:00',NULL,NULL,FALSE,3,3),
												('ok123123','2020-01-01 10:20:00','2020-05-14 10:20:00',NULL,FALSE,3,1),
												('okasdad','2020-01-01 10:20:00','2020-05-14 10:20:00','2020-05-15 10:20:00',TRUE,1,4);
