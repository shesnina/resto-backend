class RestoResponse {
  constructor(resto) {
    this.restaurant_name = resto.restaurant_name;
    this.restaurant_address = resto.restaurant_address;
    this.price_range = resto.price_range;
    this.deleted_at = resto.deleted_at;
    this.rating_id = resto.rating_id;
    this.rating = resto.rating;
    this.food_category_name = resto.food_category_name;
  }
}

module.exports = RestoResponse;
