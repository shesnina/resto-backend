const userController = require("../../controllers/user.controllers");
const userService = require("../../services/user.service");

jest.mock("../../services/user.service", () => ({
  getUsers: jest.fn(),
  getUserById: jest.fn(),
  addUser: jest.fn(),
  updateUser: jest.fn(),
  deleteUser: jest.fn(),
  getLogin: jest.fn(),
  getUserByEmail: jest.fn(),
  getPassword: jest.fn(),
  updatePassword: jest.fn(),
  forgetPassword: jest.fn(),
  updateUser: jest.fn(),
  createReview: jest.fn(),
  createRating: jest.fn(),
}));

const mockReq = (body = {}, params = {}, query = {}) => {
  return {
    body: body,
    params: params,
    query: query,
  };
};

const mockRes = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

describe("user.controller", () => {
  describe("GET /api/users/all", () => {
    it("should be return detail users ", async () => {
      const body = {
        user_name: "baharudin",
        user_email: "baharudin@loli.com",
        gender: "male",
        registered_at: "2020-01-01T12:10:25.000Z",
        user_img: "smileludin.jpg",
        role: "user",
      };
      const req = mockReq(body);
      const res = mockRes();

      userService.getUsers.mockResolvedValue(body);

      await userController.getUsers(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });

  describe("GET /api/users/:id", () => {
    it("should be return detail user for searching function ", async () => {
      const body = {
        user_id: 1,
        user_name: "baharudin",
        user_email: "baharudin@loli.com",
        gender: "male",
        registered_at: "2020-01-01T12:10:25.000Z",
        user_img: "smileludin.jpg",
        role: "user",
      };
      const query = {
        user_id: 1,
      };
      const req = mockReq(body, query);
      const res = mockRes();

      userService.getUserById.mockResolvedValue(body, query);

      await userController.getUserById(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });

  describe("POST /api/users", () => {
    it("should be return of data user ", async () => {
      const body = {
        error: "Missing required fields.",
      };
      const req = mockReq(body);
      const res = mockRes();

      userService.addUser.mockResolvedValue(body);
      await userController.addUser(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("PUT /api/users/:id", () => {
    it("should be update user", async () => {
      const body = {
        user_name: "indun",
        user_email: "celialcelau@loli.com",
        gender: "female",
        registered_at: "2020-01-03T12:10:25.000Z",
        user_img: "indun.jpg",
        role: "user",
        users_password_id: 3,
        user_password: "dun123566",
        created_at: "2020-01-03T12:10:25.000Z",
        changed_at: null,
      };
      const params = {
        id: 3,
      };
      const req = mockReq(body, params);
      const res = mockRes();

      userService.updateUser.mockResolvedValue(body, params);
      await userController.updateUser(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("DELETE /api/users/:id", () => {
    it("should be delete data of user", async () => {
      const params = {
        status: "User ID NaN succesfully deleted.",
      };
      const req = mockReq(params);
      const res = mockRes();

      userService.deleteUser.mockResolvedValue(params);
      await userController.deleteUser(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(params);
    });
  });

  describe("GET /api/users/login", () => {
    it("should be return data of login ", async () => {
      const body = {
        error: "Missing required fields.",
      };
      const req = mockReq();
      const res = mockRes();

      userService.getUserByEmail.mockResolvedValue(body);
      await userController.getLogin(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });

  describe("GET /api/users/password", () => {
    it("should be return data of update password user ", async () => {
      const body = {
        error: "Missing required fields.",
      };

      const req = mockReq();
      const res = mockRes();
      const data = { ...body };

      userService.getUserById.mockResolvedValue(data);
      await userController.updatePassword(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.json).toHaveBeenCalledWith(data);
    });
  });
  describe("PUT /api/users/forgetpassword", () => {
    it("should be return data of user forget password", async () => {
      const body = {
        error: "Missing required fields.",
      };

      const req = mockReq();
      const res = mockRes();
      const data = { ...body };

      userService.getUserByEmail.mockResolvedValue(data);
      await userController.forgetPassword(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.json).toHaveBeenCalledWith(data);
    });
  });

  describe("POST /users/review/:idResto/:idUser", () => {
    it("should be return of data resto when review", async () => {
      const body = {
        message_review: "dodi1234heyyhooooo",
      };
      const params = {
        idResto: 1,
        idUser: 21,
      };
      const req = mockReq(body, params);
      const res = mockRes();

      userService.createReview.mockResolvedValue(body, params);
      await userController.createReview(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("POST /users/rating/:idResto/:idUser", () => {
    it("should be return of data resto when rating ", async () => {
      const body = {
        rating: 5,
      };
      const params = {
        idResto: 1,
        idUser: 21,
      };
      const req = mockReq(body, params);
      const res = mockRes();

      userService.createRating.mockResolvedValue(body, params);
      await userController.createRating(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("PUT /users/review/:id", () => {
    it("should be return of data resto when updated ", async () => {
      const body = {
        message_review: "heyyooo kuyy",
      };
      const params = {
        review_id: 1,
      };
      const req = mockReq(body, params);
      const res = mockRes();

      userService.createRating.mockResolvedValue(body, params);
      await userController.createRating(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("PUT /users/rating/:idResto/:idUser", () => {
    it("should be return of data resto when updated ", async () => {
      const body = {
        rating: 5,
      };
      const params = {
        user_id: 21,
        restaurant_id: 1,
      };
      const req = mockReq(body, params);
      const res = mockRes();

      userService.createRating.mockResolvedValue(body, params);
      await userController.createRating(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
});
