const adminUserController = require("../../controllers/admin.user.controllers");
const adminUserService = require("../../services/admin.user.service");

jest.mock("../../services/admin.user.service", () => ({
  getUsers: jest.fn(),
  findbyName: jest.fn(),
  addUser: jest.fn(),
  updateUser: jest.fn(),
  deleteUser: jest.fn(),
  getUserById: jest.fn(),
}));

const mockReq = (body = {}, params = {}, query = {}) => {
  return {
    body: body,
    params: params,
    query: query,
  };
};

const mockRes = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

describe("admin.user.controller", () => {
  describe("GET /api/admin/users/", () => {
    it("should be return detail an user name ", async () => {
      const body = {
        user_name: "baharudin",
        user_email: "baharudin@loli.com",
        gender: "male",
        registered_at: "2020-01-01T12:10:25.000Z",
        user_img: "smileludin.jpg",
        role: "user",
      };
      const query = { user_name: `%baharudin%` };
      const req = mockReq(body, query);
      const res = mockRes();

      adminUserService.findbyName.mockResolvedValue(body, query);

      await adminUserController.findbyName(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("GET /api/admin/users/all", () => {
    it("should be return detail users ", async () => {
      const body = {
        user_name: "baharudin",
        user_email: "baharudin@loli.com",
        gender: "male",
        registered_at: "2020-01-01T12:10:25.000Z",
        user_img: "smileludin.jpg",
        role: "user",
      };
      const req = mockReq(body);
      const res = mockRes();

      adminUserService.getUsers.mockResolvedValue(body);

      await adminUserController.getUsers(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("GET /api/admin/users/:id", () => {
    it("should be return detail an user id ", async () => {
      const body = {
        user_name: "baharudin",
        user_email: "baharudin@loli.com",
        gender: "male",
        registered_at: "2020-01-01T12:10:25.000Z",
        user_img: "smileludin.jpg",
        role: "user",
      };
      const params = {
        used_id: 1,
      };

      const req = mockReq(body, params);
      const res = mockRes();

      adminUserService.getUserById.mockResolvedValue(body, params);

      await adminUserController.getUserById(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("POST /api/admin/users", () => {
    it("should be return of data user when post it ", async () => {
      const body = {
        user_name: "baharudin",
        user_email: "baharudin@loli.com",
        gender: "male",
        user_img: "smileludin.jpg",
        role: "user",
        user_password: "baharudinjadikaya",
      };
      const req = mockReq(body);
      const res = mockRes();

      adminUserService.addUser.mockResolvedValue(body);
      await adminUserController.addUser(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("PUT /api/admin/users/:id", () => {
    it("should be update user", async () => {
      const body = {
        user_name: "indun",
        user_email: "celialcelau@loli.com",
        gender: "female",
        registered_at: "2020-01-03T12:10:25.000Z",
        user_img: "indun.jpg",
        role: "user",
        users_password_id: 3,
        user_password: "dun123566",
        created_at: "2020-01-03T12:10:25.000Z",
        changed_at: null,
      };
      const params = {
        id: 3,
      };
      const req = mockReq(body, params);
      const res = mockRes();

      adminUserService.updateUser.mockResolvedValue(body, params);
      await adminUserController.updateUser(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("DELETE /api/admin/users/:id", () => {
    it("should be delete data of user", async () => {
      const params = {
        status: "Success deleted user with id : NaN",
      };
      const req = mockReq(params);
      const res = mockRes();

      adminUserService.deleteUser.mockResolvedValue(params);
      await adminUserController.deleteUser(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(params);
    });
  });
});
