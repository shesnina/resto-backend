const restoController = require("../../controllers/restaurant.controller");
const restoService = require("../../services/restaurant.service");

jest.mock("../../services/restaurant.service", () => ({
  findByNameOrAddress: jest.fn(),
  createResto: jest.fn(),
  updateResto: jest.fn(),
  deleteResto: jest.fn(),
  getAllRestaurants: jest.fn(),
  getAllResto: jest.fn(),
}));

const mockReq = (body = {}, params = {}, query = {}) => {
  return {
    body: body,
    params: params,
    query: query,
  };
};

const mockRes = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

describe("resto.controller", () => {
  describe("GET /api/restaurants", () => {
    it("should be return detail resto for searching function ", async () => {
      const body = {
        restaurant_id: 3,
        restaurant_name: "LIMA ENAM9",
        restaurant_email: "abcd@acb.com",
        restaurant_password: "ookkk123456",
        restaurant_address:
          "Located in the heart of the city, on the 56th floor of BCA12366 Tower - Grand Indonesia Shopping Town, Thamrin.",
        restaurant_phone: "021-981234",
        restaurant_about:
          "SKYE is established to be a Lifestyle Resort in the Sky and an iconic landmark for the city, providing a unique atmosphere for a quick getaway for Jakartans from their daily hectic lives. ",
      };
      const query = {
        searchName: `%LIMA%`,
        searchAddress: `%BCA12366%`,
      };
      const req = mockReq(body, query);
      const res = mockRes();

      restoService.findByNameOrAddress.mockResolvedValue(body, query);

      await restoController.findByNameOrAddress(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("POST /api/restaurants", () => {
    it("should be return of data resto for create resto", async () => {
      const body = {
        restaurant_name: "LIMA ENAM9",
        restaurant_email: "abcd@acb.com",
        restaurant_password: "ookkk123456",
        restaurant_address:
          "Located in the heart of the city, on the 56th floor of BCA Tower - Grand Indonesia Shopping Town, Thamrin.",
        restaurant_phone: "021-981234",
        restaurant_about:
          "SKYE is established to be a Lifestyle Resort in the Sky and an iconic landmark for the city, providing a unique atmosphere for a quick getaway for Jakartans from their daily hectic lives. ",
        restaurant_facility:
          "Reservations, Private Dining, Seating, Parking Available, Validated Parking, Valet Parking, Wheelchair Accessible, Serves Alcohol, Full Bar, Accepts Mastercard, Accepts Visa, Cash Only, Free Wifi, Accepts Credit Cards, Table Service, Live Music",
        oprational_time:
          "Office hours  10 AM - 10 PM Weekend 10 AM - 10 PM Public Holidays 10 AM  - 8 PM",
        price_range: 400000,
        created_at: "2020-01-01T12:10:25.000Z",
        updated_at: null,
        deleted_at: null,
        is_deleted: true,
        food_category_id: 1,
      };
      const req = mockReq(body);
      const res = mockRes();

      restoService.createResto.mockResolvedValue(body);
      await restoController.createResto(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("PUT /api/restaurants/:id", () => {
    it("should be update resto", async () => {
      const body = {
        restaurant_name: "LIMA ENAM9",
        restaurant_email: "abcd@acb.com",
        restaurant_password: "ookkk123456",
        restaurant_address:
          "Located in the heart of the city, on the 56th floor of BCA Tower - Grand Indonesia Shopping Town, Thamrin.",
      };
      const params = {
        id: 3,
      };
      const req = mockReq(body, params);
      const res = mockRes();

      restoService.updateResto.mockResolvedValue(body, params);
      await restoController.updateResto(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("DELETE /api/restaurants/:id", () => {
    it("should be delete data of resto", async () => {
      const params = {
        error: "sorry we could not find the id",
      };
      const req = mockReq(params);
      const res = mockRes();

      restoService.deleteResto.mockResolvedValue(params);
      await restoController.deleteResto(req, res);
      expect(res.status).toHaveBeenCalledWith(404);
      expect(res.json).toHaveBeenCalledWith(params);
    });
  });
  describe("GET /api/restaurants/all", () => {
    it("should be return data of resto ", async () => {
      const body = {
        restaurant_id: 3,
        restaurant_name: "LIMA ENAM9",
        restaurant_email: "abcd@acb.com",
        restaurant_password: "ookkk123456",
        restaurant_address:
          "Located in the heart of the city, on the 56th floor of BCA12366 Tower - Grand Indonesia Shopping Town, Thamrin.",
        restaurant_phone: "021-981234",
        restaurant_about:
          "SKYE is established to be a Lifestyle Resort in the Sky and an iconic landmark for the city, providing a unique atmosphere for a quick getaway for Jakartans from their daily hectic lives. ",
        restaurant_facility:
          "Reservations, Private Dining, Seating, Parking Available, Validated Parking, Valet Parking, Wheelchair Accessible, Serves Alcohol, Full Bar, Accepts Mastercard, Accepts Visa, Cash Only, Free Wifi, Accepts Credit Cards, Table Service, Live Music",
        oprational_time:
          "Office hours  10 AM - 10 PM Weekend 10 AM - 10 PM Public Holidays 10 AM  - 8 PM",
        price_range: 400000,
        created_at: "2020-01-01T12:10:25.000Z",
        updated_at: null,
        deleted_at: null,
        is_deleted: true,
        food_category_id: 1,
        food_category_name: "Vegetarian",
        message_review: "ok123123",
        date_review: "2020-01-01T03:20:00.000Z",
        update_review: "2020-05-14T03:20:00.000Z",
        rating: 2,
        date_rating: "2019-12-31T17:00:00.000Z",
        update_rating: null,
        user_name: "baharudin",
      };
      const req = mockReq();
      const res = mockRes();

      restoService.getAllRestaurants.mockResolvedValue(body);

      await restoController.getAllRestaurants(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(body);
    });
  });
  describe("GET /home/allresto/", () => {
    it("should be return data of resto ", async () => {
      const body = {
        error: "internal server error",
      };
      const query = { page: 1, limit: 3, search: `%ENAM9%` };

      const req = mockReq();
      const res = mockRes();
      const data = { ...body };

      restoService.getAllResto.mockResolvedValue(query);
      await restoController.getAllResto(req, res);

      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.json).toHaveBeenCalledWith(data);
    });
  });
});
