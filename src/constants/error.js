const ERROR_CONSTANT = {
    RESTAURANTS_NOT_FOUND: "restaurant not found",
    INTERNAL_SERVER_ERROR: "internal server error"
}

module.exports = ERROR_CONSTANT
